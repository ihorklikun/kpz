﻿using System;
using System.Collections.Generic;
using KlikunIhor.RobotChallenge;
using Robot.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot = Robot.Common.Robot;

namespace UnitTest.RobotChallenge
{


    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// хід до найближчої станції
        /// </summary>
        [TestMethod]
        public void MoveTest_TwoStations()
        {
            Map map = new Map();

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();

            EnergyStation st1 = new EnergyStation() { Energy = 500, Position = new Position(1, 1), RecoveryRate = 10 };
            EnergyStation st2 = new EnergyStation() { Energy = 500, Position = new Position(4, 0), RecoveryRate = 10 };
            map.Stations.Add(st1);
            map.Stations.Add(st2);

            var robots = new List<global::Robot.Common.Robot>();
            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(4, 5) });

            var res = algorithm.DoStep(robots, 0, map);
            
            Assert.AreEqual(new Position(1, 1), (res as MoveCommand).NewPosition);
            
        }


        /// <summary>
        /// Хід в сторону де більше станцій
        /// </summary>
        [TestMethod]
        public void MoveTest_ChooseSide()
        {

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();

            Map map = new Map();

            EnergyStation st1 = new EnergyStation() { Energy = 500, Position = new Position(0, 1), RecoveryRate = 10 };
            EnergyStation st2 = new EnergyStation() { Energy = 500, Position = new Position(3, 0), RecoveryRate = 10 };
            EnergyStation st3 = new EnergyStation() { Energy = 500, Position = new Position(7, 0), RecoveryRate = 10 };


            map.Stations.Add(st1);
            map.Stations.Add(st2);
            map.Stations.Add(st3);

            var robots = new List<global::Robot.Common.Robot>();

            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(5, 3) });

            var res = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position(3, 0), (res as MoveCommand).NewPosition);

        }


        /// <summary>
        /// Хід до станції біля котрої немає робота
        /// </summary>
        [TestMethod]
        public void MoveTest_ChooseStation()
        {

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();

            Map map = new Map();

            EnergyStation st1 = new EnergyStation() { Energy = 500, Position = new Position(0, 0), RecoveryRate = 10 };
            EnergyStation st2 = new EnergyStation() { Energy = 500, Position = new Position(2, 0), RecoveryRate = 10 };


            map.Stations.Add(st1);
            map.Stations.Add(st2);

            var robots = new List<global::Robot.Common.Robot>();

            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(2, 2) });
            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(3, 0) });


            var res = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position(0, 0), (res as MoveCommand).NewPosition);

        }


        /// <summary>
        /// Хід на вільну станцію
        /// </summary>
        [TestMethod]
        public void MoveTest_ChooseFreeStation()
        {

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();

            Map map = new Map();

            EnergyStation st1 = new EnergyStation() { Energy = 500, Position = new Position(0, 0), RecoveryRate = 10 };
            EnergyStation st2 = new EnergyStation() { Energy = 500, Position = new Position(2, 0), RecoveryRate = 10 };


            map.Stations.Add(st1);
            map.Stations.Add(st2);

            var robots = new List<global::Robot.Common.Robot>();

            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(2, 2) });
            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(0, 0) });


            var res = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position(2, 0), (res as MoveCommand).NewPosition);

        }


        /// <summary>
        /// Хід під кінець гри коли немає близько вигідних станцій
        /// </summary>
        [TestMethod]
        public void MoveTest_FinishGame()
        {

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();

            Map map = new Map();

            var robots = new List<global::Robot.Common.Robot>();

            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(5, 5) });


            var res = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(robots[0].Position, (res as MoveCommand).NewPosition);

        }


        /// <summary>
        /// Створення робота
        /// </summary>
        [TestMethod]
        public void CreateRobotTest_Round10()
        {

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();
            algorithm.round = 10;
            Map map = new Map();

            var robots = new List<global::Robot.Common.Robot>();

            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(5, 5) });


            var res = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(res is CreateNewRobotCommand);

        }


        /// <summary>
        /// хід до вигіднішої станції
        /// </summary>
        [TestMethod]
        public void MoveTest_ChooseRichestStation()
        {
            Map map = new Map();

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();

            EnergyStation st1 = new EnergyStation() { Energy = 500, Position = new Position(0, 0), RecoveryRate = 10 };
            EnergyStation st2 = new EnergyStation() { Energy = 800, Position = new Position(4, 0), RecoveryRate = 10 };
            map.Stations.Add(st1);
            map.Stations.Add(st2);

            var robots = new List<global::Robot.Common.Robot>();
            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(2, 3) });

            var res = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position(4, 0), (res as MoveCommand).NewPosition);

        }


        /// <summary>
        /// Чи збирає
        /// </summary>
        [TestMethod]
        public void CollectingTest()
        {
            Map map = new Map();

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();

            EnergyStation st1 = new EnergyStation() { Energy = 500, Position = new Position(0, 0), RecoveryRate = 10 };
            EnergyStation st2 = new EnergyStation() { Energy = 800, Position = new Position(4, 0), RecoveryRate = 10 };
            map.Stations.Add(st1);
            map.Stations.Add(st2);

            var robots = new List<global::Robot.Common.Robot>();
            robots.Add(new global::Robot.Common.Robot() { Energy = 500, OwnerName = algorithm.Author, Position = new Position(0, 0) });

            var res = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(res is CollectEnergyCommand);

        }

        /// <summary>
        /// Хід від пустої станції
        /// </summary>
        [TestMethod]
        public void MoveFromEmptyStationTest()
        {
            Map map = new Map();

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();

            EnergyStation st1 = new EnergyStation() { Energy = 0, Position = new Position(0, 0), RecoveryRate = 10 };
            EnergyStation st2 = new EnergyStation() { Energy = 800, Position = new Position(4, 0), RecoveryRate = 10 };
            map.Stations.Add(st1);
            map.Stations.Add(st2);

            var robots = new List<global::Robot.Common.Robot>();
            robots.Add(new global::Robot.Common.Robot() { Energy = 100, OwnerName = algorithm.Author, Position = new Position(0, 0) });

            var res = algorithm.DoStep(robots, 0, map);
            
            Assert.AreEqual(map.Stations[1].Position,(res as MoveCommand).NewPosition);
            //Assert.IsTrue(res is MoveCommand);

        }

        /// <summary>
        /// Створення робота в кінці
        /// </summary>
        [TestMethod]
        public void CreateRobotTest_40Round()
        {
            Map map = new Map();

            KlikunIhorAlgorithm algorithm = new KlikunIhorAlgorithm();
            algorithm.round = 41;

            EnergyStation st1 = new EnergyStation() { Energy = 0, Position = new Position(0, 0), RecoveryRate = 10 };
            EnergyStation st2 = new EnergyStation() { Energy = 800, Position = new Position(4, 0), RecoveryRate = 10 };
            map.Stations.Add(st1);
            map.Stations.Add(st2);

            var robots = new List<global::Robot.Common.Robot>();
            robots.Add(new global::Robot.Common.Robot() { Energy = 700, OwnerName = algorithm.Author, Position = new Position(2, 3) });

            var res = algorithm.DoStep(robots, 0, map);

            Assert.IsFalse(res is CreateNewRobotCommand);

        }

    }
}
