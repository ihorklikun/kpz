﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Events
{
    public class ProductArgs: EventArgs
    {
        public Product Product { get; set; }
        public int CountBefore { get; set; }
        public int CountAfter { get; set; }

    }
}
