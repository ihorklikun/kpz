﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Events
{
    public class Storage
    {
        public List<Product> Products { get; set; }

        public delegate void CheckStorageHandler(object sender, string message);
        public event CheckStorageHandler CheckNotify;

        private delegate void UpdateStorageHandler(object sender, ProductArgs args);
        private event UpdateStorageHandler UpdateNotify;


        public Storage()
        {
            Products = new List<Product>();
        }

        public void CheckStorage()
        {
            var list = Products.FindAll(x => x.Count == 0);
            foreach (var product in list)
            {
                CheckNotify?.Invoke(this,$"{product.Name} is out of stock");
            }
        }


        public void UpdateStorage(int minNum, int numToAdd)
        {
            var list = Products.FindAll(x => x.Count == minNum);
            foreach (var product in list)
            {
                product.Count += numToAdd;
                UpdateNotify?.Invoke(this,
                    new ProductArgs()
                    {
                        CountBefore = product.Count - numToAdd,
                        CountAfter = product.Count,
                        Product = product
                    });
            }
        }


        public void AddUpdateNotifications()
        {
            this.UpdateNotify += UpdateStorage_Notify;
        }
        public void RemoveUpdateNotifications()
        {
            this.UpdateNotify -= UpdateStorage_Notify;
        }

        private static void UpdateStorage_Notify(object sender, ProductArgs args)
        {
            Console.WriteLine($"Product Name:{args.Product.Name} Count Before:{args.CountBefore} Count After:{args.CountAfter}");
        }
    }
}
