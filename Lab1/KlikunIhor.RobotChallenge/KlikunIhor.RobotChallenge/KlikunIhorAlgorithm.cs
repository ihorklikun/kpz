﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace KlikunIhor.RobotChallenge
{
    public class KlikunIhorAlgorithm: IRobotAlgorithm
    {
        private List<AdvancedRobot> _advancedRobots = new List<AdvancedRobot>();
        public int round = 0;

        public KlikunIhorAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            round++;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];

            if (!_advancedRobots.Exists(x=>x.Robot.Position == movingRobot.Position))
            {
                _advancedRobots.Add(new AdvancedRobot(){Robot = movingRobot});
            }
            else if (_advancedRobots.First(x => x.Robot.Position == movingRobot.Position).PositionToGoing != null)
            {
                
                var advancedRobot = _advancedRobots.First(x => x.Robot.Position == movingRobot.Position);

                if (advancedRobot.PositionToGoing == movingRobot.Position)
                {
                    advancedRobot.PositionToGoing = null;
                    return new CollectEnergyCommand();
                }
                else
                {
                    return new MoveCommand() { NewPosition = advancedRobot.PositionToGoing };
                }
            }

            Position stationPosition = DistanceHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, robots);
            var station = map.Stations.First(x => x.Position == stationPosition);
            if (movingRobot.Position == stationPosition)
            {
                var advancedRobot = _advancedRobots.First(x => x.Robot.Position == movingRobot.Position);
                if (movingRobot.Energy >=500 && round < 25 &&(robots.Count < map.Stations.Count))
                {
                    advancedRobot.PositionToGoing = DistanceHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, robots,stationPosition);
                    advancedRobot.CreatedRobot = true;
                    return new CreateNewRobotCommand();
                }
                else
                {
                    if (station.Energy == 0)
                    {
                        advancedRobot.StationsNotToGo.Add(movingRobot.Position);
                        stationPosition = DistanceHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, robots, advancedRobot.StationsNotToGo);
                        return new MoveCommand() { NewPosition = stationPosition };
                    }
                    return new CollectEnergyCommand();
                }
            }
            else
            {
                return new MoveCommand() { NewPosition = stationPosition };
            }
        }

        public string Author => "Klikun Ihor";
    }
}
