﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Events
{
    class Program
    {
        static void Main(string[] args)
        {
            Storage s = new Storage();
            s.Products.Add(new Product(){Count = 0,Name = "a1"});
            s.Products.Add(new Product() { Count = 1, Name = "a2" });
            s.Products.Add(new Product() { Count = 2, Name = "a3" });
            s.Products.Add(new Product() { Count = 0, Name = "a4" });
            
            s.CheckNotify += Storage_Notify;
            s.AddUpdateNotifications();
            
            s.CheckStorage();
            s.UpdateStorage(0,20);

            s.RemoveUpdateNotifications();
            s.UpdateStorage(20,10);

        }

        private static void Storage_Notify(object sender, string message)
        {
            Console.WriteLine(message);
        }

        
    }
}
