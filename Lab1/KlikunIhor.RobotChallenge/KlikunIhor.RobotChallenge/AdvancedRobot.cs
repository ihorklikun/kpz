﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace KlikunIhor.RobotChallenge
{
    public class AdvancedRobot
    {
        public Robot.Common.Robot Robot { get; set; }
        public Position PositionToGoing { get; set; }

        public List<Position> StationsNotToGo = new List<Position>();

        public bool CreatedRobot { get; set; } = false;
    }
}
