﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Events
{
    public class Product
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
